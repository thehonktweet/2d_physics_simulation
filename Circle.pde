/*
  The Honk-Tweet
  Code by Wolfgang Gil, based on 
  
  The Nature of Code
  <http://www.shiffman.net/teaching/nature>
  Box2DProcessing example
*/

class Circle {

  // We need to keep track of a Body and a width and height
  Body body;
  float r;
  color col;
  int _circle_stroke_weight = 4;


  // Constructor
  Circle(float x, float y, float r_) {
    r = r_;
    // This function puts the particle in the Box2d world
    makeBody(x, y, r);
    body.setUserData(this);
    col = color(0);
  }

  // This function removes the particle from the box2d world
  void killBody() {
    _box2d.destroyBody(body);
  }

  boolean contains(float x, float y) {
    Vec2 worldPoint = _box2d.coordPixelsToWorld(x, y);
    Fixture f = body.getFixtureList();
    boolean inside = f.testPoint(worldPoint);
    return inside;
  }
  
  boolean isClicked(int mouse_x, int mouse_y)
  {
    Vec2 pos = _box2d.getBodyPixelCoord(body);
    if( sqrt( pow((mouse_x - pos.x),2)+pow((mouse_y - pos.y),2) ) <= r )
    {
      return true;
    }
    return false;
  }

 void display(boolean selected) {
    // We look at each body and get its screen position
    Vec2 pos = _box2d.getBodyPixelCoord(body);
    // Get its angle of rotation
    float a = body.getAngle();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(a);
    fill(col);
    if(selected)
    {
      
      stroke(color(255, 204, 0)); 
    }
    else stroke(255);
    
    strokeWeight(_circle_stroke_weight);
    ellipse(0, 0, r*2, r*2);
    // Let's add a line so we can see the rotation
    line(0, 0, r, 0);
    popMatrix();
  }

   // Here's our function that adds the particle to the Box2D world
  void makeBody(float x, float y, float r) {
    // Define a body
    BodyDef bd = new BodyDef();
    // Set its position
    bd.position = _box2d.coordPixelsToWorld(x, y);
    bd.type = BodyType.DYNAMIC;
    bd.linearDamping = 1.1f;
    bd.angularDamping = 1.0f;
    body = _box2d.createBody(bd);

    // Make the body's shape a circle
    CircleShape cs = new CircleShape();
    cs.m_radius = _box2d.scalarPixelsToWorld(r);

    FixtureDef fd = new FixtureDef();
    fd.shape = cs;
    // Parameters that affect physics
    fd.density = 50;
    fd.friction = 1;
    fd.restitution = 0.3;

    // Attach fixture to body
    body.createFixture(fd);
    

    //body.setAngularVelocity(random(-10, 10));
  }
  
  void applyForce(Vec2 v) {
    body.applyForce(v, body.getWorldCenter());
  }
}

 
