/*
  The Honk-Tweet
  Code by Wolfgang Gil, based on 
  
  The Nature of Code
  <http://www.shiffman.net/teaching/nature>
  Box2DProcessing example
  
  Library used: Box2D
  to import:
  1) click on Sketch -> import library -> add library
  2) in the search bar type box2d
  3) install "Box2D for Processing" by Daniel Shiffman
  
  ENJOY!
  
*/
 
import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import org.jbox2d.dynamics.contacts.*;

// A reference to our box2d world
Box2DProcessing _box2d;

int _num_circles = 10;
int _circle_diameter = 80;
// Just a single box this time
//Circle circle1, circle2;

// An ArrayList of particles that will fall on the surface
ArrayList<Circle> _circle_array;

// A list we'll use to track fixed objects
ArrayList<Boundary> boundaries;

Spring _spring;
int _circle_selector = -1;
//boolean _circle_selected = false;

// The Spring that will attach to the box from the mouse
//Spring spring1, spring2;

void setup() {
  fullScreen();
  smooth();

  // Initialize box2d physics and create the world
  _box2d = new Box2DProcessing(this);
 
  _box2d.createWorld();
  _box2d.setGravity(0, 0);

  // Add a listener to listen for collisions!
  //_box2d.world.setContactListener(new CustomListener());

  _circle_array = new ArrayList<Circle>();
  //_spring_array = new ArrayList<Spring>();
  
  for(int i = 0; i < _num_circles; i++)
  {
    int circle_x = int( random(200, width - 200 ) );
    int circle_y = int( random(200, height - 200 ) );
    
   // print(circle_x);
    
   Circle circle = new Circle( circle_x, circle_y, _circle_diameter );
    
   // Make the spring (it doesn't really get initialized until the mouse is clicked)
   _circle_array.add(circle);
    
  }
  
  boundaries = new ArrayList<Boundary>();
  
  // Add a bunch of fixed boundaries
 
  //top wall
  boundaries.add(new Boundary(3, 3,width*2,6));
  //floor wall
  boundaries.add(new Boundary(3, height-3,width*2,6));
  //left wall
  boundaries.add(new Boundary(width-3,height/2,6,height));
  //right wall
  boundaries.add(new Boundary(3,height/2,6, height));
}

void draw() {
  
  background(0);

   // We must always step through time!
  _box2d.step();

  if (mousePressed) {
    if(_circle_selector == -1)
    {
      for(int i = 0; i < _num_circles; i++)
      {
        Circle circle = _circle_array.get(i);
        if(circle.isClicked(mouseX, mouseY))
        {
          _circle_selector = i;
         // _circle_selected = true;
         _spring = new Spring();
         _spring.bind(mouseX, mouseY, circle);
        }
      }
    }
    else
    {
        //Spring spring = _spring_array.get(_circle_selector);
        _spring.update(mouseX,mouseY);
          //spring.display();*/
    }
  }
  else
  {
    if(_circle_selector != -1)
    {
      _circle_selector = -1;
     _spring.destroy();
    }
  }
  
  for(int i = 0; i < _num_circles; i++)
  {
    if(i == _circle_selector)
    {
      _circle_array.get(i).display(true);
    }
    else _circle_array.get(i).display(false);
  }

  
  // Display all the boundaries
  for (Boundary wall: boundaries) {
    wall.display();
  }
}
